//
//  CakeCell.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "CakeCell.h"
#import "CakeDataStore.h"
#import "CakeItem.h"

@implementation CakeCell

- (void)configureCellWithCakeAtIndexPath:(NSIndexPath *)idxPath fromStore:(CakeDataStore *)cakeStore {
    CakeItem *cake = cakeStore.cakeList[idxPath.row];
    NSString *cakeTitle = [cake.title capitalizedString];
    NSString *cakeId = cakeTitle;
    self.titleLabel.text = cakeTitle;
    self.descriptionLabel.text = [cake.longDescription lowercaseString];
    
    __weak typeof(self) weakSelf = self;
    [cakeStore loadImageForCake:cake completion:^(NSData *imageData,NSError *error) {
        
        if ([cakeId isEqualToString:weakSelf.titleLabel.text]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *image;
                if (imageData) {
                    image = [UIImage imageWithData:imageData];
                }else{
                    image = [UIImage imageNamed:@"cakePlaceholder"];
                }
                
                [weakSelf.cakeImageView setImage:image];
                
            });
        }else{
            //the cell is now being reused by another cake. So we do not set the image that corresponded to the old cake using this cell.
        }

    }];
     
}

@end
