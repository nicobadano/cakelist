//
//  CakeDataStore.h
//  Cake List
//
//  Created by Nicolas Badano on 8/1/17.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kNewCakesAvailableNotification;
extern NSString *const kNetworkErrorNotification;

@class CakeItem;

@interface CakeDataStore : NSObject

@property (nonatomic,strong,readonly) NSArray<CakeItem *> *cakeList;

- (void)goGetCakes;
- (void)loadImageForCake:(CakeItem *)cake completion:(void(^)(NSData *imageData,NSError *error))completion;

@end
