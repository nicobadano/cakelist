//
//  CakeDataStore.m
//  Cake List
//
//  Created by Nicolas Badano on 8/1/17.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import "CakeDataStore.h"
#import "CakeItem.h"

NSString *const kNewCakesAvailableNotification = @"kNewCakesAvailableNotification";
NSString *const kNetworkErrorNotification = @"kNetworkErrorNotification";

@interface CakeDataStore ()

@property (nonatomic,strong) NSArray<CakeItem *> *cakeList;
@property (nonatomic,strong) NSCache *cakeImageCache;

@end

@implementation CakeDataStore

- (instancetype)init
{
    self = [super init];
    if (self) {
        _cakeList = @[];
        _cakeImageCache = [[NSCache alloc] init];
        [self downloadCakes];
    }
    return self;
}

- (void)downloadCakes {
    NSURL *url = [NSURL URLWithString:@"https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json"];
    __weak typeof(self) weakSelf = self;
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              
                                              if (!error && data) {
                                                  NSError *jsonError;
                                                  NSArray *cakeArray = [NSJSONSerialization
                                                                     JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                     error:&jsonError];
                                                  if (!jsonError){
                                                      
                                                      NSMutableArray<CakeItem *> *newCakes = [[NSMutableArray alloc] init];
                                                      for (NSDictionary *cakeDic in cakeArray) {
                                                          CakeItem *cake = [[CakeItem alloc] init];
                                                          cake.title = cakeDic[@"title"];
                                                          cake.longDescription = cakeDic[@"desc"];
                                                          cake.imageURL = [NSURL URLWithString:cakeDic[@"image"]];
                                                          [newCakes addObject:cake];
                                                      }
                                                      weakSelf.cakeList = newCakes;
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [[NSNotificationCenter defaultCenter] postNotificationName:kNewCakesAvailableNotification object:nil];
                                                      });
                                                  }
                                              }else{
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkErrorNotification object:nil];
                                                  });
                                              }
                                              
                                          }];
    [downloadTask resume];
}

- (void)goGetCakes {
    [self downloadCakes];
}

- (void)loadImageForCake:(CakeItem *)cake completion:(void(^)(NSData *imageData,NSError *error))completion {
    NSURL *url = cake.imageURL;
    
    NSData *imgData = [self.cakeImageCache objectForKey:url];
    
    if (imgData) {
        completion(imgData,nil);
    }else{
        __weak typeof(self) weakSelf = self;
        NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                              dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                  
                                                  if (!error) {
                                                      if (data) {
                                                          [weakSelf.cakeImageCache setObject:data forKey:url];
                                                      }
                                                      completion(data,nil);
                                                  }else{
                                                      completion(nil,error);
                                                  }
                                                  
                                                  
                                                  
                                              }];
        [downloadTask resume];
    }
}

@end
