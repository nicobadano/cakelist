//
//  MasterViewController.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "MasterViewController.h"
#import "CakeCell.h"
#import "CakeDataStore.h"

@interface MasterViewController ()
@property (strong, nonatomic) CakeDataStore *cakeDatastore;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //register to the new cakes arrived notification. Posted when the Cake datastore retrives new data.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newCakesArrived:) name:kNewCakesAvailableNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNetworkError:) name:kNetworkErrorNotification object:nil];
    
    //Ideally we would want to inject this dependency to the MasterViewController
    _cakeDatastore = [[CakeDataStore alloc] init];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 79;

}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger numOfSections = 0;
    
    if (self.cakeDatastore.cakeList.count > 0){
        numOfSections = 1;
        tableView.backgroundView = nil;
    }else{
        UILabel *noDataLabel= [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 50)];
        noDataLabel.text = @"No cakes available :(";
        noDataLabel.textColor = [UIColor blackColor];
        noDataLabel.font = [UIFont fontWithName:@"Verdana" size:18.0f];
        noDataLabel.textAlignment = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
    }
    
    return numOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cakeDatastore.cakeList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CakeCell *cell = (CakeCell*)[tableView dequeueReusableCellWithIdentifier:@"CakeCell"];
    
    [cell configureCellWithCakeAtIndexPath:indexPath fromStore:self.cakeDatastore];
        
    return cell;
}

- (IBAction)refreshTable:(id)sender {
    [self.cakeDatastore goGetCakes];
}

#pragma mark - CakeDataStore Observer

- (void)newCakesArrived:(NSNotification *)notif {
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (void)onNetworkError:(NSNotification *)notif {
    [self.refreshControl endRefreshing];
}

@end
