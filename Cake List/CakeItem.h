//
//  CakeItem.h
//  Cake List
//
//  Created by Nicolas Badano on 8/1/17.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CakeItem : NSObject

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *longDescription;
@property (nonatomic,strong) NSURL *imageURL;

@end
